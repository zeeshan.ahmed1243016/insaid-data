Answer_Key = { 
              'Dewvrak12@hotmail.com' : ['hotmail', 'gmail', 'yahoo', 'asia'],

              'kspiteri@outlook.com' : ['outlook', 'sbcglobal', 'me', 'aol', 'verizon', 'yahoo', 'gmail', 'msn', 'live'],

              'He' : {'He': 3, 'Once': 1, 'again': 1, 'and': 1, 'back': 2, 'but': 1, 'car': 1, 'drove': 1, 'forgot': 1, 'found': 1, 'grab': 1,
                      'grocery': 2, 'he': 3, 'his': 1, 'home': 1, 'inside': 1, 'it': 2, 'raced': 3, 'realized': 1, 'store': 2, 'the': 3,
                      'to': 4, 'wallet': 1, 'went': 1},
              
              'Fear' : {'Fear': 1, 'and': 3, 'anger': 2, 'conflict': 2, 'hatred': 2, 'leads': 4, 'suffering': 1, 'to': 4},

              'Now' : {'Gods': 1, 'Now': 4, 'a': 1, 'all': 1, 'and': 1, 'brotherhood': 1, 'children': 1, 'dark': 1, 'democracy': 1, 'desolate': 1,
                       'for': 1, 'from': 2, 'injustice': 1, 'is': 4, 'justice': 2, 'lift': 1, 'make': 2, 'nation': 1, 'of': 6, 'our': 1, 'path': 1,
                      'promises': 1, 'quicksands': 1, 'racial': 2, 'real': 1, 'reality': 1, 'rise': 1, 'rock': 1, 'segregation': 1, 'solid': 1,
                       'sunlit': 1, 'the': 9, 'time': 4, 'to': 6, 'valley': 1},
              
              'borrow' : 'rotator',

              'murdrum' : 'rotavator',

              'deified' : 'deified'
              }


#Comaprison1
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_UnqiueDomains_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input.split()[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


#Comaprison2
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_CountFreq_TestCase(func, Input):
  
  #Correct Output
  Correct_output = Answer_Key[Input.split()[0]]

  #Running yout function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

#Comparison3
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
def Run_HighestScorePalindrome_TestCase(func, Input):

  #Correct Output
  Correct_output = Answer_Key[Input.split()[0]]

  #Running your function
  Your_output = func(Input)

  #Checking your ouput
  if Your_output == Correct_output:
    print(f'Test Case Pass: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
    
  else:
    print(f'Test Case Failed: \n\nCorrect Output: {Correct_output} \nYour Output: {Your_output}')
#''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
