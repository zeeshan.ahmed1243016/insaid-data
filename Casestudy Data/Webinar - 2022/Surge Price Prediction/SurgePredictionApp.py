import streamlit as st
import pandas as pd
from xgboost import XGBClassifier
from PIL import Image
import pickle

im = Image.open('/content/App_Icon.png')
st.set_page_config(layout="wide", page_title="Surge Price Prediction App", page_icon = im)

#App Header
st.write("""
# Surge Price Prediction
#### This app predicts the **Surge Price Multiplier** for your cab ride!
""")

#Setting up Input format
def user_input_features():

  #First row two columns
  left, right = st.columns(2)
  with left:
    Type_of_Cab = st.radio('Type of Cab you want to Book', options=['Mini', 'Sedan', 'XL', 'Premium', 'Rental'], horizontal=True)
  with right:
    Weather_Condition = st.radio('Condition of Weather in Ride Location', options=['Clear', 'Rain', 'Heavy Rain'], horizontal=True)
  
  #Third row two columns
  left, right = st.columns(2)
  with left:
    Trip_Distance = st.slider('Distance you want to travel (Km)',1, 100, 42, 2)
  with right:
    Customer_Rating = st.slider('Whats your Rating on the system?', 0.0, 5.0, 2.4, 0.2)

  #Fourth row two columns
  left, right = st.columns(2)
  with left:
    Cancellation_Last_1Month = st.slider('How many rides have you cancelled in the last month?',0, 20, 2, 1)
  with right:
    pass
  
  inputs = {'Trip_Distance' : Trip_Distance,
            'Weather_Condition' : Weather_Condition,
            'Type_of_Cab' : Type_of_Cab,
            'Customer_Rating' : Customer_Rating,
            'Cancellation_Last_1Month' : Cancellation_Last_1Month}

  return inputs

#Taking Inputs
inputs = user_input_features()
df = pd.DataFrame(inputs, index=['Parameters'])

#Showing DataFrame
st.write(f'### User Input parameters:')
st.dataframe(data=df, width=1400, height=50)

#Loading Model
with open('Saved_Model.pkl', 'rb') as file:
  Model = pickle.load(file)

#Load Transformer
with open('Encoder.pkl', 'rb') as f:
  enc = pickle.load(f)

#Transforming data
cat_cols = ['Type_of_Cab', 'Weather_Condition']
df_cat = df[cat_cols]
df_cat = pd.DataFrame(enc.transform(df_cat).toarray(), columns = enc.get_feature_names_out(), index=['Parameters'])

#Joining Cats and Conts
df.drop(columns=cat_cols, inplace=True)
df = pd.concat([df, df_cat], axis=1)

#Taking Predicton
cols = ['Trip_Distance', 'Customer_Rating', 'Cancellation_Last_1Month', 'Type_of_Cab_Premium', 'Type_of_Cab_Rental', 
        'Type_of_Cab_Sedan', 'Type_of_Cab_XL', 'Weather_Condition_Heavy Rain', 'Weather_Condition_Rain']

df = df[cols].reset_index(drop=True)
prediction = Model.predict(df)

#Write prediction
st.write(f"""
### Prediction from the Model:
### {prediction[0]}""")
