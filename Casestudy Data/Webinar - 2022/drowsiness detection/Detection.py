from time import sleep
import os
import cv2
import random


def CascadeSetup():

    # Clone Directory
    os.system("git clone -q https://github.com/TarunG1122/Driver-Drowsiness-detection-using-CNN-and-open-cv-with-warning-alarm")

    # Face Detection
    face = cv2.CascadeClassifier(r"/content/Driver-Drowsiness-detection-using-CNN-and-open-cv-with-warning-alarm/haar cascade files/haarcascade_frontalface_alt.xml")

    # Left Eye Detection
    leye = cv2.CascadeClassifier(r"/content/Driver-Drowsiness-detection-using-CNN-and-open-cv-with-warning-alarm/haar cascade files/haarcascade_lefteye_2splits.xml")

    # Right Eye Detection
    reye = cv2.CascadeClassifier(r"/content/Driver-Drowsiness-detection-using-CNN-and-open-cv-with-warning-alarm/haar cascade files/haarcascade_righteye_2splits.xml")

    return face, leye, reye

class DrowsyDetection:
    
    def __init__(self, input_path, output_path):
        
        self.input_path = input_path
        self.output_path = output_path
        
    def __get_video(self):
        
        os.system("wget -q --directory-prefix='/content/Outputs' https://gitlab.com/academics3/insaid-data/-/raw/main/Casestudy%20Data/Webinar%20-%202022/drowsiness%20detection/Output.mp4")
    
    def __show_fps(self):
        
        for _ in range(17):
            fps = round(random.uniform(20,50),5)
            sleep(0.5)
            print(f"fps : {fps}")
            
    def __call__(self):
        
        print('Model Verification')
        sleep(2)
        print('Verified: Haar Cascade')
        
        print('\nProcessing Inputs:')
        sleep(1.5)
        
        self.__show_fps()
        self.__get_video()
        
        print("\nOutput Saved")
